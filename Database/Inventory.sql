USE [master]
GO
/****** Object:  Database [Inventario]    Script Date: 03/09/2020 9:08:10 PM ******/
CREATE DATABASE [Inventario]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Inventario', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Inventario.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Inventario_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Inventario_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Inventario] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Inventario].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Inventario] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Inventario] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Inventario] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Inventario] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Inventario] SET ARITHABORT OFF 
GO
ALTER DATABASE [Inventario] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Inventario] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Inventario] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Inventario] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Inventario] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Inventario] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Inventario] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Inventario] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Inventario] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Inventario] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Inventario] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Inventario] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Inventario] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Inventario] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Inventario] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Inventario] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Inventario] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Inventario] SET RECOVERY FULL 
GO
ALTER DATABASE [Inventario] SET  MULTI_USER 
GO
ALTER DATABASE [Inventario] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Inventario] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Inventario] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Inventario] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Inventario] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Inventario', N'ON'
GO
ALTER DATABASE [Inventario] SET QUERY_STORE = OFF
GO
USE [Inventario]
GO
/****** Object:  User [DESKTOP-722N163\MLLOPART]    Script Date: 03/09/2020 9:08:11 PM ******/
CREATE USER [DESKTOP-722N163\MLLOPART] FOR LOGIN [DESKTOP-722N163\MLLOPART] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[Almacen]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Almacen](
	[Id_Almacen] [int] IDENTITY(0,1) NOT NULL,
	[Nomb_Almacen] [varchar](64) NOT NULL,
	[Id_Estados] [int] NOT NULL,
 CONSTRAINT [Almacen_pk] PRIMARY KEY CLUSTERED 
(
	[Id_Almacen] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contables_Accounts]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contables_Accounts](
	[Id] [int] NOT NULL,
	[Name] [varchar](64) NOT NULL,
 CONSTRAINT [Contables_Accounts_pk] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cuentas_Contables]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cuentas_Contables](
	[Id_CuentasC] [int] IDENTITY(0,1) NOT NULL,
	[Nomb_CuntasC] [varchar](64) NOT NULL,
 CONSTRAINT [Cuentas_Contables_pk] PRIMARY KEY CLUSTERED 
(
	[Id_CuentasC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Estados]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estados](
	[Id_Estado] [int] IDENTITY(0,1) NOT NULL,
	[Nomb_estado] [varchar](64) NOT NULL,
 CONSTRAINT [Id_Estados_pk] PRIMARY KEY CLUSTERED 
(
	[Id_Estado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Existences]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Existences](
	[Id] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Warehouses_Id] [int] NOT NULL,
	[Products_Id] [int] NOT NULL,
 CONSTRAINT [Existences_pk] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Existencias]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Existencias](
	[Id_Existencia] [int] IDENTITY(0,1) NOT NULL,
	[Id_productos] [int] NOT NULL,
	[Id_Almacen] [int] NOT NULL,
	[Cant_Existencia] [int] NOT NULL,
 CONSTRAINT [Existencias_pk] PRIMARY KEY CLUSTERED 
(
	[Id_Existencia] ASC,
	[Id_productos] ASC,
	[Id_Almacen] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inventary_Types]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inventary_Types](
	[Id] [int] NOT NULL,
	[Names] [varchar](64) NOT NULL,
	[Contables_Accounts_Id] [int] NOT NULL,
	[Statuses_Id] [int] NOT NULL,
 CONSTRAINT [Inventary_Types_pk] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Productos](
	[Id_productos] [int] IDENTITY(0,1) NOT NULL,
	[Nomb_Productos] [varchar](120) NOT NULL,
	[Existencia] [int] NULL,
	[Costo_Producto] [int] NOT NULL,
	[Id_estado] [int] NOT NULL,
	[Id_TInventario] [int] NOT NULL,
 CONSTRAINT [Productos_pk] PRIMARY KEY CLUSTERED 
(
	[Id_productos] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Id] [int] NOT NULL,
	[Name] [varchar](120) NOT NULL,
	[Cost] [int] NOT NULL,
	[Inventary_Types_Id] [int] NOT NULL,
	[Statuses_Id] [int] NOT NULL,
 CONSTRAINT [Products_pk] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Statuses]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Statuses](
	[Id] [int] NOT NULL,
	[Name] [varchar](64) NOT NULL,
 CONSTRAINT [Statuses_pk] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tipo_inventario]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tipo_inventario](
	[Id_Tinventario] [int] IDENTITY(0,1) NOT NULL,
	[Nomb_tInventario] [varchar](64) NOT NULL,
	[Id_Estados] [int] NOT NULL,
	[Id_CuentasC] [int] NOT NULL,
 CONSTRAINT [Tipo_inventario_pk] PRIMARY KEY CLUSTERED 
(
	[Id_Tinventario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tipo_Transaccion]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tipo_Transaccion](
	[Id_TTrans] [int] IDENTITY(0,1) NOT NULL,
	[Nomb_TTrans] [varchar](64) NOT NULL,
 CONSTRAINT [Tipo_Transaccion_pk] PRIMARY KEY CLUSTERED 
(
	[Id_TTrans] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Transaccion]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaccion](
	[Id_Trans] [int] IDENTITY(0,1) NOT NULL,
	[Id_TTrans] [int] NOT NULL,
	[Id_productos] [int] NOT NULL,
	[Fech_Trans] [datetime] NOT NULL,
	[Cant_Trans] [int] NOT NULL,
	[Costo_Trans] [int] NOT NULL,
 CONSTRAINT [Transaccion_pk] PRIMARY KEY CLUSTERED 
(
	[Id_Trans] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Transaction_Types]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction_Types](
	[Id] [int] NOT NULL,
	[Name] [varchar](64) NOT NULL,
 CONSTRAINT [Transaction_Types_pk] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Transactions]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transactions](
	[Id] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Quantity] [int] NOT NULL,
	[product_cost] [numeric](18, 0) NOT NULL,
	[TransType_ID] [int] NOT NULL,
	[Productos_Id] [int] NOT NULL,
	[From_WareHouse] [int] NOT NULL,
	[To_Warehouse] [int] NOT NULL,
 CONSTRAINT [Transactions_pk] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User_Types]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Types](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Names] [varchar](64) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Names] [varchar](64) NOT NULL,
	[User_Types_id] [int] NOT NULL,
	[Password] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warehouses]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warehouses](
	[Id] [int] NOT NULL,
	[Names] [varchar](64) NOT NULL,
	[Statuses_Id] [int] NOT NULL,
 CONSTRAINT [Warehouses_pk] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Almacen]  WITH CHECK ADD  CONSTRAINT [Almacen_Id_Estados] FOREIGN KEY([Id_Estados])
REFERENCES [dbo].[Estados] ([Id_Estado])
GO
ALTER TABLE [dbo].[Almacen] CHECK CONSTRAINT [Almacen_Id_Estados]
GO
ALTER TABLE [dbo].[Existences]  WITH CHECK ADD  CONSTRAINT [Existences_Products] FOREIGN KEY([Products_Id])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[Existences] CHECK CONSTRAINT [Existences_Products]
GO
ALTER TABLE [dbo].[Existences]  WITH CHECK ADD  CONSTRAINT [Existences_Warehouses] FOREIGN KEY([Warehouses_Id])
REFERENCES [dbo].[Warehouses] ([Id])
GO
ALTER TABLE [dbo].[Existences] CHECK CONSTRAINT [Existences_Warehouses]
GO
ALTER TABLE [dbo].[Existencias]  WITH CHECK ADD  CONSTRAINT [Existencias_Almacen] FOREIGN KEY([Id_Almacen])
REFERENCES [dbo].[Almacen] ([Id_Almacen])
GO
ALTER TABLE [dbo].[Existencias] CHECK CONSTRAINT [Existencias_Almacen]
GO
ALTER TABLE [dbo].[Existencias]  WITH CHECK ADD  CONSTRAINT [Existencias_fk] FOREIGN KEY([Id_productos])
REFERENCES [dbo].[Productos] ([Id_productos])
GO
ALTER TABLE [dbo].[Existencias] CHECK CONSTRAINT [Existencias_fk]
GO
ALTER TABLE [dbo].[Inventary_Types]  WITH CHECK ADD  CONSTRAINT [Inventary_Types_Contables_Accounts] FOREIGN KEY([Contables_Accounts_Id])
REFERENCES [dbo].[Contables_Accounts] ([Id])
GO
ALTER TABLE [dbo].[Inventary_Types] CHECK CONSTRAINT [Inventary_Types_Contables_Accounts]
GO
ALTER TABLE [dbo].[Inventary_Types]  WITH CHECK ADD  CONSTRAINT [Inventary_Types_Statuses] FOREIGN KEY([Statuses_Id])
REFERENCES [dbo].[Statuses] ([Id])
GO
ALTER TABLE [dbo].[Inventary_Types] CHECK CONSTRAINT [Inventary_Types_Statuses]
GO
ALTER TABLE [dbo].[Productos]  WITH CHECK ADD  CONSTRAINT [Productos_Id_Estados] FOREIGN KEY([Id_estado])
REFERENCES [dbo].[Estados] ([Id_Estado])
GO
ALTER TABLE [dbo].[Productos] CHECK CONSTRAINT [Productos_Id_Estados]
GO
ALTER TABLE [dbo].[Productos]  WITH CHECK ADD  CONSTRAINT [Productos_Tipo_inventario] FOREIGN KEY([Id_TInventario])
REFERENCES [dbo].[Tipo_inventario] ([Id_Tinventario])
GO
ALTER TABLE [dbo].[Productos] CHECK CONSTRAINT [Productos_Tipo_inventario]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [Products_Inventary_Types] FOREIGN KEY([Inventary_Types_Id])
REFERENCES [dbo].[Inventary_Types] ([Id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [Products_Inventary_Types]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [Products_Statues] FOREIGN KEY([Statuses_Id])
REFERENCES [dbo].[Statuses] ([Id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [Products_Statues]
GO
ALTER TABLE [dbo].[Tipo_inventario]  WITH CHECK ADD  CONSTRAINT [Tipo_inventario_Cuentas_Contables] FOREIGN KEY([Id_CuentasC])
REFERENCES [dbo].[Cuentas_Contables] ([Id_CuentasC])
GO
ALTER TABLE [dbo].[Tipo_inventario] CHECK CONSTRAINT [Tipo_inventario_Cuentas_Contables]
GO
ALTER TABLE [dbo].[Tipo_inventario]  WITH CHECK ADD  CONSTRAINT [Tipo_inventario_Id_Estados] FOREIGN KEY([Id_Estados])
REFERENCES [dbo].[Estados] ([Id_Estado])
GO
ALTER TABLE [dbo].[Tipo_inventario] CHECK CONSTRAINT [Tipo_inventario_Id_Estados]
GO
ALTER TABLE [dbo].[Transaccion]  WITH CHECK ADD  CONSTRAINT [Transaccion_Productos] FOREIGN KEY([Id_productos])
REFERENCES [dbo].[Productos] ([Id_productos])
GO
ALTER TABLE [dbo].[Transaccion] CHECK CONSTRAINT [Transaccion_Productos]
GO
ALTER TABLE [dbo].[Transaccion]  WITH CHECK ADD  CONSTRAINT [Transaccion_Tipo_Transaccion] FOREIGN KEY([Id_TTrans])
REFERENCES [dbo].[Tipo_Transaccion] ([Id_TTrans])
GO
ALTER TABLE [dbo].[Transaccion] CHECK CONSTRAINT [Transaccion_Tipo_Transaccion]
GO
ALTER TABLE [dbo].[Transactions]  WITH CHECK ADD  CONSTRAINT [From_warehouse] FOREIGN KEY([From_WareHouse])
REFERENCES [dbo].[Warehouses] ([Id])
GO
ALTER TABLE [dbo].[Transactions] CHECK CONSTRAINT [From_warehouse]
GO
ALTER TABLE [dbo].[Transactions]  WITH CHECK ADD  CONSTRAINT [To_Warehouses] FOREIGN KEY([To_Warehouse])
REFERENCES [dbo].[Warehouses] ([Id])
GO
ALTER TABLE [dbo].[Transactions] CHECK CONSTRAINT [To_Warehouses]
GO
ALTER TABLE [dbo].[Transactions]  WITH CHECK ADD  CONSTRAINT [Transactions_Productos] FOREIGN KEY([Productos_Id])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[Transactions] CHECK CONSTRAINT [Transactions_Productos]
GO
ALTER TABLE [dbo].[Transactions]  WITH CHECK ADD  CONSTRAINT [Transactions_Transaction_Types] FOREIGN KEY([TransType_ID])
REFERENCES [dbo].[Transaction_Types] ([Id])
GO
ALTER TABLE [dbo].[Transactions] CHECK CONSTRAINT [Transactions_Transaction_Types]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [User_types_id] FOREIGN KEY([User_Types_id])
REFERENCES [dbo].[User_Types] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [User_types_id]
GO
ALTER TABLE [dbo].[Warehouses]  WITH CHECK ADD  CONSTRAINT [Warehouses_Statues] FOREIGN KEY([Statuses_Id])
REFERENCES [dbo].[Statuses] ([Id])
GO
ALTER TABLE [dbo].[Warehouses] CHECK CONSTRAINT [Warehouses_Statues]
GO
/****** Object:  StoredProcedure [dbo].[Insert_Transfer]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Insert_Transfer](@PROD INT,@QUANTITY INT,@COST FLOAT,@WAREHOUSE1 INT,@WAREHOUSE2 INT)
AS
DECLARE @ERROR INT;
BEGIN
TRAN
INSERT INTO DBO.Transactions (ID,Productos_Id,Date,Quantity,product_cost)
values (3,@PROD,CONVERT(DATE,GETDATE()),@QUANTITY,@COST)

UPDATE DBO.Existences
SET DBO.Existences.Quantity = DBO.Existences.Quantity - @QUANTITY
WHERE DBO.Existences.Products_Id = @PROD
AND DBO.Existences.Warehouses_Id = @WAREHOUSE1

UPDATE DBO.Existences
SET DBO.Existences.Quantity = DBO.Existences.Quantity + @QUANTITY
WHERE DBO.Existences.Products_Id = @PROD
AND DBO.Existences.Warehouses_Id = @WAREHOUSE2

SET @ERROR = @@ERROR

IF (@ERROR <> 0)
BEGIN 
ROLLBACK TRAN
PRINT 'ERROR EN LA OPERACION'
END
GO
/****** Object:  StoredProcedure [dbo].[Insertar_almacen]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Insertar_almacen] (@NOMBRE VARCHAR(64),@ESTADO INT)
AS
BEGIN
INSERT INTO DBO.Almacen (Nomb_Almacen,Id_Estados)
values (@NOMBRE,@ESTADO)
END
GO
/****** Object:  StoredProcedure [dbo].[Insertar_cuentas_contables]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Insertar_cuentas_contables] (@NOMBRE VARCHAR(64))
AS
BEGIN
INSERT INTO DBO.Cuentas_Contables (Nomb_CuntasC)
values (@NOMBRE)
END
GO
/****** Object:  StoredProcedure [dbo].[Insertar_entrada]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Insertar_entrada](@PRODUCTO INT,@CANTIDAD INT,@COSTO FLOAT,@ALMACEN INT)
AS
BEGIN
INSERT INTO DBO.Transaccion (Id_TTrans,Id_productos,Fech_Trans,Cant_Trans,Costo_Trans)
values (1,@PRODUCTO,CONVERT(DATE,GETDATE()),@CANTIDAD,@COSTO)

UPDATE DBO.Existencias
SET DBO.Existencias.Cant_Existencia = DBO.Existencias.Cant_Existencia + @CANTIDAD
WHERE DBO.Existencias.Id_productos = @PRODUCTO
AND DBO.Existencias.Id_Almacen = @ALMACEN
END
GO
/****** Object:  StoredProcedure [dbo].[Insertar_estado]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Insertar_estado] (@NOMBRE VARCHAR(64))
AS
BEGIN
INSERT INTO DBO.Estados (Nomb_estado)
values (@NOMBRE)
END
GO
/****** Object:  StoredProcedure [dbo].[Insertar_producto]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Insertar_producto] (@NOMBRE VARCHAR(120),@EXISTENCIA int,@COSTO FLOAT,@ESTADO INT,@TIPO_INVENTARIO INT)
AS
BEGIN
INSERT INTO DBO.Productos (Nomb_Productos,Existencia,Costo_Producto,Id_estado,Id_TInventario)
values (@NOMBRE,@EXISTENCIA,@COSTO,@ESTADO,@TIPO_INVENTARIO)
END
GO
/****** Object:  StoredProcedure [dbo].[Insertar_salida]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Insertar_salida](@PRODUCTO INT,@CANTIDAD INT,@COSTO FLOAT,@ALMACEN INT)
AS
BEGIN
INSERT INTO DBO.Transaccion (Id_TTrans,Id_productos,Fech_Trans,Cant_Trans,Costo_Trans)
values (2,@PRODUCTO,CONVERT(DATE,GETDATE()),@CANTIDAD,@COSTO)

UPDATE DBO.Existencias
SET DBO.Existencias.Cant_Existencia = DBO.Existencias.Cant_Existencia - @CANTIDAD
WHERE DBO.Existencias.Id_productos = @PRODUCTO
AND DBO.Existencias.Id_Almacen = @ALMACEN
END
GO
/****** Object:  StoredProcedure [dbo].[Insertar_tipo_inventario]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Insertar_tipo_inventario](@NOMBRE VARCHAR(64),@ESTADO INT,@Cuentas int)
AS
BEGIN
INSERT INTO DBO.Tipo_inventario (Nomb_tInventario,Id_Estados,Id_CuentasC)
values (@NOMBRE,@ESTADO,@Cuentas)
END
GO
/****** Object:  StoredProcedure [dbo].[Insertar_tipo_transaccion]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Insertar_tipo_transaccion](@NOMBRE VARCHAR(64))
AS
BEGIN
INSERT INTO DBO.Tipo_Transaccion (Nomb_TTrans)
values (@NOMBRE)
END
GO
/****** Object:  StoredProcedure [dbo].[Insertar_transferencia]    Script Date: 03/09/2020 9:08:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Insertar_transferencia](@PRODUCTO INT,@CANTIDAD INT,@COSTO FLOAT,@ALMACEN1 INT,@ALMACEN2 INT)
AS
BEGIN
INSERT INTO DBO.Transaccion (Id_TTrans,Id_productos,Fech_Trans,Cant_Trans,Costo_Trans)
values (3,@PRODUCTO,CONVERT(DATE,GETDATE()),@CANTIDAD,@COSTO)

UPDATE DBO.Existencias
SET DBO.Existencias.Cant_Existencia = DBO.Existencias.Cant_Existencia - @CANTIDAD
WHERE DBO.Existencias.Id_productos = @PRODUCTO
AND DBO.Existencias.Id_Almacen = @ALMACEN1

UPDATE DBO.Existencias
SET DBO.Existencias.Cant_Existencia = DBO.Existencias.Cant_Existencia + @CANTIDAD
WHERE DBO.Existencias.Id_productos = @PRODUCTO
AND DBO.Existencias.Id_Almacen = @ALMACEN2

END
GO
USE [master]
GO
ALTER DATABASE [Inventario] SET  READ_WRITE 
GO
