﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventario;
using Inventario_App.DAL;
using System.Data.Entity;


namespace Inventario_App.Services
{
    class ProductsVm
    {
        MyContext _context = new MyContext();

        public List<Product> GetProducts() => _context.Product
                .Include(x => x.Status)
                .Include(x => x.Inventary_Type)
                .ToList();

        public Product GetProduct(int id) => _context.Product.FirstOrDefault(x=>x.Id==id);

        public void AddProduct(Product product){

            _context.Product.Add(product);
            _context.SaveChanges();
            
        }

        public void editProduct(Product newProd) {
            Product oldProd = _context.Product.FirstOrDefault(x => x.Id == newProd.Id);

            if (oldProd != null)
            {
                _context.Entry(oldProd).CurrentValues.SetValues(newProd);
                _context.SaveChanges();

            }
        }

        internal void DeleteProduct(int id)
        {
            if ( id > 0) {
                _context.Product.Remove(_context.Product.FirstOrDefault(x => x.Id == id));
                _context.SaveChanges();
            }
        }
    }
}
