﻿using Inventario_App.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventario_App.Services
{
    public class InventoryTypeVm
    {
        private MyContext _context = new MyContext();
        public InventoryTypeVm() { }

        public List<Inventary_Type> GetInventory_Types() => _context.Inventary_Type.ToList();

    }
}
