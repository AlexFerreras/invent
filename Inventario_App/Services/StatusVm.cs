﻿using Inventario_App.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventario_App.Services
{
    public class StatusVm
    {
        MyContext _context = new MyContext();
        public StatusVm() { }

        public List<Status> GetStatuses() => _context.Status.ToList();

    }
}
