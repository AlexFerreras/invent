﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventario_App.DAL;
using System.Data.Entity;

namespace Inventario_App.Services
{
    public class ExistenceWarehouseVm
    {
        MyContext _context = new MyContext();

        public ExistenceWarehouseVm() { }

        public void WarehouseExistenceInit() {

           var existences = _context.Existence
                .Include(x => x.Warehouse)
                .Include(x=>x.Product)
                .ToList();

           new Inventario.FrmExistenciasAlmacen(existences).ShowDialog();

        }

        


    }
}
