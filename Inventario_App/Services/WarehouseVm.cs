﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventario_App.DAL;

namespace Inventario_App.Services
{
    public class WarehouseVm
    {
        MyContext _context = new MyContext();

        public void AddWarehouse(Warehouse warehouse) {
            
                _context.Warehouse.Add(warehouse);
            
        }

        public List<Warehouse> GetWarehouse() {
            return _context.Warehouse.ToList();
        }

        public Warehouse GetWarehouse(int ID)
        {
            return _context.Warehouse.Where(x=>x.Id==ID).FirstOrDefault();
        }

        public void RemoveWarehouse(int ID)  {

             _context.Warehouse.Remove(_context.Warehouse.Find(ID));
     
        }





    }
}
