﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inventario
{
    public partial class FrmAlmacenes : Form
    {
        public FrmAlmacenes()
        {
            InitializeComponent();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void articulosToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void cmdAgregar_Click(object sender, EventArgs e)
        {
            Form AgregarAlmacen = new FrmAgregarAlmacen();
            AgregarAlmacen.Show();
        }

        private void FrmAlmacenes_Load(object sender, EventArgs e)
        {

        }
    }
}
