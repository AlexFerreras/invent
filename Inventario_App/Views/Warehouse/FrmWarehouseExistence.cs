﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Inventario_App.DAL;

namespace Inventario
{
    public partial class FrmExistenciasAlmacen : Form
    {
        private readonly List<Existence> _existences;
        public FrmExistenciasAlmacen(List<Existence> existences)
        {
            InitializeComponent();
            _existences = existences;
        }

        private void FrmExistenciasAlmacen_Load(object sender, EventArgs e)
        {
            DGVexistence.DataSource = _existences;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
