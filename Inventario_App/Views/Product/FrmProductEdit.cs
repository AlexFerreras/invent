﻿using Inventario_App.DAL;
using Inventario_App.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inventario
{
    public partial class FrmProductEdit : Form
    {
        private Product _product;
        private InventoryTypeVm _inventoryTypeService = new InventoryTypeVm();
        private StatusVm _statusService = new StatusVm();
        private ProductsVm _productService = new ProductsVm();

        public FrmProductEdit()
        {
           
        }

        public FrmProductEdit(Product product)
        {
            _product = product;
            InitializeComponent();
            chargeDropDowns();
            SetInputsValues();

        }
        public void SetInputsValues() {

            txtProductId.Text = _product.Id.ToString();
            txtProductCost.Value = _product.Cost;
            txtProductName.Text = _product.Name;
            //cbProductStatus.SelectedIndex = _product.Statuses_Id;
            
        }

        private void chargeDropDowns()
        {

            cbProductStatus.DataSource = _statusService.GetStatuses();
            cbProductStatus.ValueMember = "Id";
            cbProductStatus.DisplayMember = "Name";

            cbProductInventory.DataSource = _inventoryTypeService.GetInventory_Types();
            cbProductInventory.ValueMember = "Id";
            cbProductInventory.DisplayMember = "Name";
        }
        private void FrmEditarArticulo_Load(object sender, EventArgs e)
        {

        }

        private void cmdGuardar_Click(object sender, EventArgs e)
        {

            try
            {
                Product newProduct = new Product
                {
                    Id = int.Parse(txtProductId.Text),
                    Name = Name = txtProductName.Text,
                    Cost = int.Parse(txtProductCost.Value.ToString()),
                    Statuses_Id = int.Parse(cbProductStatus.SelectedValue.ToString()),
                    Inventary_Types_Id = int.Parse(cbProductInventory.SelectedValue.ToString())
                };

                _productService.editProduct(newProduct);

                MessageBox.Show("El producto se Edito correctamente");
                this.Close();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);

            }

        }

        private void cmdCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("SI o NO", "Esta seguro que desea borrar el Producto?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
                {
                    _productService.DeleteProduct(int.Parse(txtProductId.Text));
                    this.Close();
                }
                
                
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);

            }

        }
    }
}
