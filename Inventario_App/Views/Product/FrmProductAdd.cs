﻿using Inventario_App.DAL;
using Inventario_App.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inventario
{
    public partial class FrmProductAdd : Form
    {
        private List<Status> _statuses;
        private List<Inventary_Type> _inventoryTypes;
        private ProductsVm _productService = new ProductsVm();
        private InventoryTypeVm _inventoryTypeService = new InventoryTypeVm();
        private StatusVm _statusService = new StatusVm();
        public FrmProductAdd()
        {
            _statuses = _statusService.GetStatuses();
            _inventoryTypes = _inventoryTypeService.GetInventory_Types();
            InitializeComponent();
            chargeDropDowns();
        }

        private void chargeDropDowns()
        {
            cbStatus.DataSource = _statuses;
            cbStatus.ValueMember = "Id";
            cbStatus.DisplayMember = "Name";
          
            cbInventory.DataSource = _inventoryTypes;
            cbInventory.ValueMember = "Id";
            cbInventory.DisplayMember = "Name";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void FrmAgregarArtituculo_Load(object sender, EventArgs e)
        {

        }

        private void cmdGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                 Product product = new Product {
                     Name = txtName.Text,
                     Cost = int.Parse(txtCost.Text),
                     Statuses_Id = int.Parse(cbStatus.SelectedValue.ToString()),
                     Inventary_Types_Id= int.Parse(cbInventory.SelectedValue.ToString())
                 };

              _productService.AddProduct(product);
             
               MessageBox.Show("El producto se agrego correctamente");
               this.Close();
             
               
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
               
            }
            
        }
    }
}
