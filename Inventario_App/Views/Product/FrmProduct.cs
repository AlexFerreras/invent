﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Inventario_App.DAL;
using Inventario_App.Services;

namespace Inventario
{
    public partial class FrmProduct : Form
    {
        private List<Product> _products;
        private ProductsVm _productService = new ProductsVm();

        public FrmProduct()
        {
            InitializeComponent();
            loadData();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void articulosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        public void loadData() {

            _products = _productService.GetProducts();
            DataTable table = new DataTable("Products");
            table.Columns.Add("ID");
            table.Columns.Add("Nombre");
            table.Columns.Add("Costo");
            table.Columns.Add("Inventario");
            table.Columns.Add("Estado");


            foreach (Product product in this._products)
            {

                table.Rows.Add(product.Id, product.Name, product.Cost, (product.Inventary_Type != null) ? product.Inventary_Type.Name : "--", product.Status.Name);

            }

            DGVProducts.DataSource = table;
            DGVProducts.Refresh();
        }
        private void FrmArticulos_Load(object sender, EventArgs e)
        {
            loadData();
        }

        private void salirToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gestionToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void tipoDeInventarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form TipoInventario = new FrmTipoInventario();
            TipoInventario.Show();
        }

        private void almacenesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form Almacenes = new FrmAlmacenes();
            Almacenes.Show();
        }

        private void cmdAgregar_Click(object sender, EventArgs e)
        {
            new FrmProductAdd().ShowDialog();
           
        }

        private void cmdEditar_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = this.DGVProducts.Rows[0];
            int id = int.Parse(row.Cells[0].Value.ToString());
            new FrmProductEdit(_productService.GetProduct(id)).ShowDialog();
        }

        private void DGVProducts_DoubleClick(object sender, EventArgs e)
        {
           


        }

        private void articulosToolStripMenuItem_Click_1(object sender, EventArgs e)
        {

        }

        private void FrmProduct_Activated(object sender, EventArgs e)
        {
            loadData();
        }

        private void DGVProducts_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void DGVProducts_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
            try{             

                DataGridViewRow row = this.DGVProducts.Rows[e.RowIndex];
                int id = int.Parse(row.Cells[0].Value.ToString());
                new FrmProductEdit(_productService.GetProduct(id)).ShowDialog();
            }
            catch (Exception ex)
            {

               MessageBox.Show(ex.Message);
            }
            
        }

        private void cmdEliminar_Click(object sender, EventArgs e)
        {

            DataGridViewRow row = this.DGVProducts.Rows[this.DGVProducts.CurrentCell.RowIndex];
            int id = int.Parse(row.Cells[0].Value.ToString());
            _productService.DeleteProduct(id);

        }
    }
}
