//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Inventario_App.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Transaction
    {
        public int Id { get; set; }
        public System.DateTime Date { get; set; }
        public int Quantity { get; set; }
        public int Transaction_Type_Id { get; set; }
        public int Productos_Id { get; set; }
        public int From_WareHouse { get; set; }
        public int To_Warehouse { get; set; }
    
        public virtual Product Product { get; set; }
        public virtual Warehouse Warehouse { get; set; }
        public virtual Warehouse Warehouse1 { get; set; }
        public virtual Transaction_Type Transaction_Type { get; set; }
    }
}
