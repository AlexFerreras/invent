
create database inventory;-- Table: Transaction_Types
use inventory;

CREATE TABLE Transaction_Type (
    Id integer NOT NULL CONSTRAINT Transaction_Types_pk PRIMARY KEY identity(1,1),
    Name varchar(64) NOT NULL
);

CREATE TABLE Contables_Account (
    Id integer NOT NULL CONSTRAINT Contables_Accounts_pk PRIMARY KEY identity(1,1),
    Name varchar(64) NOT NULL
);

-- Table: Statuses
CREATE TABLE Status (
    Id integer NOT NULL CONSTRAINT Statuses_pk PRIMARY KEY identity(1,1),
    Name varchar(64) NOT NULL
);

-- Table: Warehouses
CREATE TABLE Warehouse (
    Id integer NOT NULL CONSTRAINT Warehouses_pk PRIMARY KEY identity(1,1),
    Name varchar(64) NOT NULL,
    Statuses_Id integer NOT NULL,
    CONSTRAINT Warehouses_Statues FOREIGN KEY (Statuses_Id)
    REFERENCES Status (Id)
);

-- Table: Inventary_Types
CREATE TABLE Inventary_Type (
    Id integer NOT NULL CONSTRAINT Inventary_Types_pk PRIMARY KEY identity(1,1),
    Name varchar(64) NOT NULL,
    Contables_Accounts_Id integer NOT NULL,
    Statuses_Id integer NOT NULL,
    CONSTRAINT Inventary_Types_Contables_Accounts FOREIGN KEY (Contables_Accounts_Id)
    REFERENCES Contables_Account (Id),
    CONSTRAINT Inventary_Types_Statuses FOREIGN KEY (Statuses_Id)
    REFERENCES Status (Id)
);

-- Table: Products
CREATE TABLE Product (
    Id integer NOT NULL CONSTRAINT Products_pk PRIMARY KEY identity(1,1),
    Name varchar(120) NOT NULL,
    Cost integer NOT NULL,
    Inventary_Types_Id integer NULL,
    Statuses_Id integer NOT NULL,
    CONSTRAINT Products_Inventary_Types FOREIGN KEY (Inventary_Types_Id)
    REFERENCES Inventary_Type (Id),
    CONSTRAINT Products_Statues FOREIGN KEY (Statuses_Id)
    REFERENCES Status (Id)
);

-- Table: Existences
CREATE TABLE Existence (
    Id integer NOT NULL CONSTRAINT Existences_pk PRIMARY KEY identity(1,1),
    Quantity integer NOT NULL,
    Warehouses_Id integer NOT NULL,
    Products_Id integer NOT NULL,
    CONSTRAINT Existences_Warehouses FOREIGN KEY (Warehouses_Id)
    REFERENCES Warehouse (Id),
    CONSTRAINT Existences_Products FOREIGN KEY (Products_Id)
    REFERENCES Product (Id)
);




-- Table: Transactions
CREATE TABLE [Transaction] (
    Id integer NOT NULL CONSTRAINT Transactions_pk PRIMARY KEY identity(1,1),
    Date datetime NOT NULL,
    Quantity integer NOT NULL,
    Transaction_Type_Id integer NOT NULL,
    Productos_Id integer NOT NULL,
    From_WareHouse integer NOT NULL,
    To_Warehouse integer NOT NULL,
    CONSTRAINT Transactions_Transaction_Types FOREIGN KEY (Transaction_Type_Id)
    REFERENCES Transaction_Type (Id),
    CONSTRAINT Transactions_Productos FOREIGN KEY (Productos_Id)
    REFERENCES Product (Id),
    CONSTRAINT From_warehouse FOREIGN KEY (From_WareHouse)
    REFERENCES Warehouse (Id),
    CONSTRAINT To_Warehouses FOREIGN KEY (To_Warehouse)
    REFERENCES Warehouse (Id)
);


-- End of file.

