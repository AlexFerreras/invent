-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2020-02-19 22:53:09.223

-- tables
-- Table: Contables_Accounts
CREATE TABLE Contables_Accounts (
    Id integer NOT NULL CONSTRAINT Contables_Accounts_pk PRIMARY KEY AUTOINCREMENT,
    Name varchar(64) NOT NULL
);

-- Table: Existences
CREATE TABLE Existences (
    Id integer NOT NULL CONSTRAINT Existences_pk PRIMARY KEY AUTOINCREMENT,
    Quantity integer NOT NULL,
    Warehouses_Id integer NOT NULL,
    Products_Id integer NOT NULL,
    CONSTRAINT Existences_Warehouses FOREIGN KEY (Warehouses_Id)
    REFERENCES Warehouses (Id),
    CONSTRAINT Existences_Products FOREIGN KEY (Products_Id)
    REFERENCES Products (Id)
);

-- Table: Inventary_Types
CREATE TABLE Inventary_Types (
    Id integer NOT NULL CONSTRAINT Inventary_Types_pk PRIMARY KEY AUTOINCREMENT,
    Names varchar(64) NOT NULL,
    Contables_Accounts_Id integer NOT NULL,
    Statuses_Id integer NOT NULL,
    CONSTRAINT Inventary_Types_Contables_Accounts FOREIGN KEY (Contables_Accounts_Id)
    REFERENCES Contables_Accounts (Id),
    CONSTRAINT Inventary_Types_Statuses FOREIGN KEY (Statuses_Id)
    REFERENCES Statuses (Id)
);

-- Table: Products
CREATE TABLE Products (
    Id integer NOT NULL CONSTRAINT Products_pk PRIMARY KEY AUTOINCREMENT,
    Name varchar(120) NOT NULL,
    Cost integer NOT NULL,
    Inventary_Types_Id integer NOT NULL,
    Statuses_Id integer NOT NULL,
    CONSTRAINT Products_Inventary_Types FOREIGN KEY (Inventary_Types_Id)
    REFERENCES Inventary_Types (Id),
    CONSTRAINT Products_Statues FOREIGN KEY (Statuses_Id)
    REFERENCES Statuses (Id)
);

-- Table: Statuses
CREATE TABLE Statuses (
    Id integer NOT NULL CONSTRAINT Statuses_pk PRIMARY KEY AUTOINCREMENT,
    Name varchar(64) NOT NULL
);

-- Table: Transaction_Types
CREATE TABLE Transaction_Types (
    Id integer NOT NULL CONSTRAINT Transaction_Types_pk PRIMARY KEY AUTOINCREMENT,
    Name varchar(64) NOT NULL
);

-- Table: Transactions
CREATE TABLE Transactions (
    Id integer NOT NULL CONSTRAINT Transactions_pk PRIMARY KEY AUTOINCREMENT,
    Date datetime NOT NULL,
    Quantity integer NOT NULL,
    product_cost numeric NOT NULL,
    TransType_ID integer NOT NULL,
    Productos_Id integer NOT NULL,
    From_WareHouse integer NOT NULL,
    To_Warehouse integer NOT NULL,
    CONSTRAINT Transactions_Transaction_Types FOREIGN KEY (TransType_ID)
    REFERENCES Transaction_Types (Id),
    CONSTRAINT Transactions_Productos FOREIGN KEY (Productos_Id)
    REFERENCES Products (Id),
    CONSTRAINT From_warehouse FOREIGN KEY (From_WareHouse)
    REFERENCES Warehouses (Id),
    CONSTRAINT To_Warehouses FOREIGN KEY (To_Warehouse)
    REFERENCES Warehouses (Id)
);

-- Table: Warehouses
CREATE TABLE Warehouses (
    Id integer NOT NULL CONSTRAINT Warehouses_pk PRIMARY KEY AUTOINCREMENT,
    Names varchar(64) NOT NULL,
    Statuses_Id integer NOT NULL,
    CONSTRAINT Warehouses_Statues FOREIGN KEY (Statuses_Id)
    REFERENCES Statuses (Id)
);

-- End of file.

